[[_TOC_]]

There are two installation options - Local or Docker.

Docker is the preferred method of installation as it resolves all dependencies and will work on Ubuntu 24.04. Running inside Docker still allows development work but does add some overhead (managing Dockerfiles and compose files, rebuilding images).

It's also possible to install everything locally if you are running Ubuntu 20.04 and ROS Noetic.


| **Installation Type** | **Ubuntu Version** |       **ROS Version Required**      |         **Development Environment?**        |
|:---------------------:|:------------------:|:-----------------------------------:|:-------------------------------------------:|
|   Docker Development  |     20.04, 24.04    | No Local ROS version required | Yes, but more complex to manage with docker |
|         Local         |        20.04       | ROS1 Noetic                         | Yes, natively.                              |


## System Prerequisites

Ubuntu is supported, either 20.04 + ROS1 Noetic, or 24.04.

Regardless of your installation path, docker is required for perception and grasp synthesis nodes, so install Docker first [Docker Installation](#docker-installation).


### Docker Installation

```bash
# Install Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Manage/Run Docker as non-root:
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

# Restart docker service
sudo systemctl restart docker

# Test installation:
docker run hello-world
```
Or, follow the official instructions [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)


## Installation (via Docker)

Currently, we offer pre-built images for the manipulation pipeline, object detection, stereo image generation, and grasp synthesis.

Because Docker manages dependencies internally, this image will run on computers that support Docker and have Nvidia GPU/CUDA capabilities.

When the containers are started in developement mode (`docker compose -f raven_manip_sw/docker/docker-compose/dev/docker-compose.yaml up -d`) - they locally mount the src/ workspace to enable editing locally. If you are running ROS1 natively, you can use traditional introspection tools (`rostopic echo`, etc.) *from outside* the running docker containers. If you are NOT running ROS1 natively, you can use traditional introspection tools *from inside* the container. In this case, GUI support is provided but you will first need to run `docker exec -it manipulation /bin/bash` to enter a terminal inside the container.

1. Setup folders and clone workspace
```bash
mkdir raven_manipulation/src -p
cd raven_manipulation/src
git clone https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw.git
```

2. Install base repos and tmux:
```bash
vcs import < raven_manip_sw/raven_manip_sw.repos --recursive && \
vcs import < manip_executive/manip_executive.repos --recursive  && \
vcs import < grasp_selection/grasp_selection.repos --recursive  && \
vcs import < raven_manip_sw/perception.repos --recursive  && \
vcs import < raven_manip_sw/mapping.repos --recursive  && \
vcs import < grasp_synthesis/grasp_synthesis.repos --recursive && \
vcs import < mapping/mapping.repos --recursive
sudo apt install tmux
```

3. Pull all docker images and models (this will take a long time):
```bash
# Required credentials to pull images:.
# You will see a warning, you can either ignore it or configure credential helper
# https://docs.docker.com/engine/reference/commandline/login/#credentials-store
docker login registry.gitlab.com

# Pull docker images
cd raven_manip_sw
./get_models_images.sh

# For RAFT Models (not required for sim):
cd ../raft_stereo/src/raft_stereo && ./download_models.sh
```

4. Enter a development environment for simulation:

```bash
# Start the docker containers:
docker compose -f raven_manip_sw/docker/docker-compose/dev/docker-compose.yaml up -d

# Start a simulation dev environment:
cd raven_manip_sw/launch/launch_scripts/
./start_sim_docker.sh

# To end the session:
# Kill tmux:
tmux kill-session -t manip_docker

# Remove containers:
docker compose -f raven_manip_sw/docker/docker-compose/dev/docker-compose.yaml down
```

You should see a tmux pane with many tabs:
![tmux_ui](../figs/tmux_ui.png)

5. You'll need to start most of the processes within the tmux windows - the commands are pre-populated. Just switch tabs and "Enter"

## Installation (Local)
To install the pipeline locally on your machine, follow these instructions.

There are two `*.repos` files in this repo that install the base dependencies (`raven_manip_sw.repos`), and the perception dependencies (`perception.repos`)

## System Prerequisites

Ubuntu 20.04 (dealer's choice on how you install this) or Ubuntu 24.04 if using the docker installation instructions.

`sudo apt install curl git openssh-client openssh-server`

### ROS

This repository assumes that you have ROS Noetic installed (though other distributions may also work). If you have not installed ROS, follow [the official instructions](http://wiki.ros.org/noetic/Installation/Ubuntu), including the step "Dependencies for Building Packages".

### Zerotier
Install Zerotier (for networking, not related to proper system performance)
* Marc's Local Network ID: a84ac5c10a09b225
* Aaron's Local Network ID: 8056c2e21c359ce8

After running the code below, Marc/Aaron will have to manually approve you on the network. Ping them with your client ID (should show up after joining, else run sudo zerotier-cli status).

```bash
curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi
```

#### Join network
* sudo zerotier-cli join a84ac5c10a09b225
* sudo zerotier-cli join 8056c2e21c359ce8

### vcstool

If you do not have `vcstool`, install it. Assuming your apt list already contains the ROS repositories:
```bash
sudo apt-get update
sudo apt-get install python3-vcstool
```

You may need to add the ROS repositories for this:
```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```

Allow tab-complete:
```bash
echo "source /usr/share/vcstool-completion/vcs.bash" >> ~/.bashrc
source ~/.bashrc
```

### rosdep

If you do not have `rosdep`, install it:
```
sudo apt install python3-rosdep
rosdep init
```

### Catkin

Install catkin + associated ROS packages. Make sure you've sourced your environment properly (`source ~/.bashrc`)
```bash
sudo apt install python3-catkin-tools python3-osrf-pycommon
pip install empy catkin-pkg pycryptodomex gnupg rospkg ipympl defusedxml PySide2
```

## Perception Prerequisites


* [Install NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html)

### CUDA

Cuda 12 is required for multiple nodes.

Install the max version available for your graphics card.

```bash
# Remove whatever lingers:
sudo apt-get purge nvidia-*
sudo apt-get update
sudo apt-get autoremove
sudo rm -rf /usr/local/cuda*

# Now install latest cuda:
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.1-1_all.deb
sudo dpkg -i cuda-keyring_1.1-1_all.deb
sudo apt-get update
sudo apt-get -y install cuda

# Install for GPU access
sudo apt install -y nvidia-cuda-toolkit nvidia-docker2
```

You'll need to reboot at this point. If you have trouble, use the info at [this link](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=20.04&target_type=deb_local) to install the CUDA Toolklit.

You need to add the following lines to your `~/.bashrc` file (where `cuda-12.3` is replaced by whatever version you've installed. Find it with `ls /usr/local | grep cuda`)

  ```bash
  export PATH=/usr/local/cuda-12.3/bin${PATH:+:${PATH}}$
  export LD_LIBRARY_PATH=/usr/local/cuda-12.3/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
  ```

After adding these lines and sourcing `~/.bashrc,` run `nvcc --version | grep "release" | awk '{print $6}' | cut -c2-` (which may require installing `sudo apt install nvidia-cuda-toolkit`) to ensure the right version of CUDA has been properly installed.

## Installation (Arm Control)

### Initialize Catkin workspace
```bash
cd ~/Documents && mkdir -p raven_manipulation/src && cd raven_manipulation
catkin init
cd src
git clone https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw.git

# Install base repos:
vcs import < raven_manip_sw/raven_manip_sw.repos --recursive
vcs import < manip_executive/manip_executive.repos --recursive
vcs import < grasp_selection/grasp_selection.repos --recursive  # needed for grasp_synthesis.launch

# Install dependencies (run from inside /src/ folder)
rosdep install --from-paths . --ignore-src -r -y

# Manually install the pyikfast wheel
cd grasp_selection/src/grasp_selection
pip3 install pyikfast-0.0.1-cp38-cp38-linux_x86_64.whl
cd ../../../

# We have a few dependencies that are not in rosdep; install them manually using pip:
pip install roboticstoolbox-python textual cmap qpsolvers[cvxopt]
sudo apt-get install tmux

# Build the packages
catkin build
source devel/setup.bash

# Build the watchdog (run from inside /src/ folder)
cd libbpl_protocol/bpl_watchdogd
mkdir build && cd build
cmake ..
make
```

| :exclamation:  tmux Tips |
|-----------------------------------------|
If you, like me, are tmux illiterate, you can make things a bit easier with the following script. My tmux installation came default with `ctrl+a` set instead of `ctrl+b` for max confusion by being different that every single tmux tutorial ever. It also adds mouse stuff, so you can select panes and tabs with a click.
```bash
# Define the new tmux configuration
TMUX_CONF_CONTENT='unbind C-a
set-option -g prefix C-b
bind-key C-b send-prefix
set -g mouse on
# split panes using | and -
bind | split-window -h
bind - split-window -v
unbind %'

# Overwrite the .tmux.conf file
echo "$TMUX_CONF_CONTENT" > ~/.tmux.conf

# Reload the tmux configuration
tmux source-file ~/.tmux.conf
```


### Install py_trees

(Do NOT install `ros-noetic-py-trees`)

```bash
cd ~/
git clone https://github.com/apl-ocean-engineering/py_trees.git
cd py_trees/
pip3 install .
```

## Installation (Perception)

### Install perception packages
```bash
# (from the src folder)
vcs import < raven_manip_sw/perception.repos --recursive

# Install grasp synthesis repos
# These could be submodules, but the management of the submodule was terrible
vcs import < grasp_synthesis/grasp_synthesis.repos --recursive

# Install dependencies
rosdep install --from-paths . --ignore-src -r -y
```

### Download model weights

For both `grasp_synthesis` and `object_detection`, you'll need to download the weights for the model you plan to use. These files are too big to commit to github, so instead are stored in [this drive folder](https://drive.google.com/drive/folders/1esUiK_Z3DkbexOwCLPuekCuUSLXdjqis?usp=sharing).

* Download tsgrasp weights to `grasp_synthesis/src/grasp_synthesis/tsgrasp/models/${FolderName}/model.ckpt`
* Download the detection weights to `object_detection/models/R{FolderName}/model.{pth, pt}`

There is only one set of weights for TSGrasp. We are experimenting with different object detection networks, so there are a few choices there:

| Network | Folder Name | Link to Weights | Comments |
|---------|-------------|-----------------|----------|
| tsgrasp | tsgrasp_scene_1_frame | [model.ckpt](https://drive.google.com/file/d/1qbhzNCGT78gAE8npMYF8HJ2ddAnewwQF/view?usp=drive_link) |  |
| detectron2 | detectron2_09_24 | [model.pth](https://drive.google.com/file/d/1B1JFdauaVIGs3H8EOGjHCI_8b9L3RhRe/view?usp=drive_link) | Retrained for fishtail and kettlebell detections in HH101 |
| yolov8 | yolov8_09_24 | [model.pt](https://drive.google.com/file/d/1vH95D37s8EQTFbWvV-hZvLef1yiBRm3O/view?usp=drive_link) | Not retrained for fishtail detections |

Within each model's folder, the `metadata.yaml` file will include information including a direct link to the associated weights file and the filename it is expected to be saved as. For example, here's one for detectron2  in the `object_detection` repository:  [detectron2_09_03/metadata.yaml](https://gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection/-/blob/main/models/detectron2_09_03/metadata.yaml?ref_type=heads):

```yaml
model_url: https://drive.google.com/drive/folders/1yY9P482pmdiQ7fK1gS5MIr-FzZGucKbJ?usp=drive_link
model_path: models/detectron2_09_03/model.pth
base_model_config: COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml
image_topic: /detection_image
confidence_threshold: 0.85
classes:
  0: kettlebell
  1: fishtail
```

You will also need to update `perception.launch` to reflect your chosen model:
* Change `detector` argument to detectron2 or yolov8
* If using non-default weights, change the `metadata_path` arg to reflect the model's folder name

### Pull or Build Docker containers

```bash
# Pull the Docker for grasp synth:
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis/grasp_synthesis:node

# OR build Docker for grasp synthesis:
cd grasp_synthesis/docker
./build.sh

# Pull the docker for object detection:
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection/detection:node

# OR Build Docker for object detection:
cd object_detection/docker
./build.sh
```

### Install ZED Dependencies (if using ZED)

1. Make sure you have CUDA 12 installed by running `nvcc --version | grep "release" | awk '{print $6}' | cut -c2-` (you may have to install this tool with `sudo apt install nvidia-cuda-toolkit`). If not, see above.

2. Make sure you're a member of the `zed` group. If not:
`sudo usermod -aG zed $USER`

3. Check if the ZED SDK has already been installed. It gets installed to `/usr/local/zed`. If it's already installed, skip to step 5.

4. To install the ZED SDK, go to https://www.stereolabs.com/developers/ and download ZED SDK for Ubuntu 20 4.0 (for CUDA 12).

```bash
cd ~/Downloads
chmod a+x ZED_SDK_Ubuntu20_cuda12.1_v4.0.8.zstd.run
./ZED_SDK_Ubuntu20_cuda12.1_v4.0.8.zstd.run
```
Accept all default options except for "optimize AI models". This will take forever -- better to just let it optimize the ones it needs the first time they're used.

5. Check that the files in zed are owned by $USER, and have group zed. If not: `sudo chown -hR $USER:zed /usr/local/zed`

Now, fix permissions issues. In order to cd into a directory, the group needs 'x' permissions.
```bash
cd /usr/local
sudo chmod -R g+wr zed
sudo chmod g+x zed
cd zed
sudo chmod g+x doc firmware include include/sl lib resources samples settings tools
```
