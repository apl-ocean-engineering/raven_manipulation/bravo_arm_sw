[[_TOC_]]

# Connecting to the Arm

To connect to the arm, you need to connect to the arm with an ethernet cable and set the host computer's IPv4 information. The arm expects the following configuration:

| Item                      | Value             |
| -----------               | -----------       |
| Subnet Mask               | 255.255.255.0     |
| Host computer IP          | 192.168.2.1       |
| Arm computer IP and port  | 192.168.2.3:6789  |

If you have multiple network interfaces (e.g., WiFi and a LAN port), then you may need to disable the other interface if the IP range interferes with the common 192.168 block.

An example Ubuntu configuration is below.

![networking](../figs/net.png)

With the network set up, you should be able to reach the arm with:
```
ping 192.168.2.3
```
> **Note**
>
> Need to connect to WiFi at the same time? Don't quote me on this, but you might be able to specify "link-local" connection in the IPv4 settings for the ethernet table, and then explicitly add a route to the arm with `sudo route add 192.168.2.3 dev enp3s0`, where `enp3s0` is the Linux network device corresponding to to ethernet visible in the output of `ip addr`. This seems brittle though since it resets when you disconnect from the arm.

# Using the Arm Software
| :exclamation:  A Caution About ARM CONNECTIONS!  |
|-----------------------------------------|
Be absolutely sure you can ping the arm prior to launching the below file AND be sure that once launched, the arm displayed on screen matches the arm position in reality. If not, when you power on/enable the arm, it may do unexpected things.

To bring up a graphical interface to control the arm, first source your workspace and then use:
```
roslaunch raven_manip_sw bravo_driver.launch simulated:=false
```



This brings up an RViz interface similar to the figure below.

![rviz](../figs/rviz.png)

## Enabling the Arm
To arm the arm, we use our patented arm-arming service. This service enables the "write" commands within `rsa_bravo_driver` that set joint positions and cause motion. **The arm motion should only be enabled when people are clear of the workspace.**

From a terminal in which you have sourced the workspace, run:
```
rosservice call /bravo/enable_bravo 1
```

If you ever wish to disable arm motion, issue:
```
rosservice call /bravo/enable_bravo 0
```
which will prevent subsequent joint positions from being commanded.

## Simulation
A configuration is also provided to simulate arm motions in Gazebo. To launch in simulation, use the `simulated` flag (enabled by default):
```
roslaunch raven_manip_sw bravo_arm.launch simulated:=true
```
This launches the same Rviz window as with `simulated:=false`, but starts a Gazebo server to simulate the arm in an empty world.

## Motion Planning
You can perform motion planning by dragging the interactive in marker, clicking '**plan**', inspecting the animated trajectory to verify that it is free of collisions, and clicking '**execute**'.

| :exclamation: Check your Trajectory!|
|-----------------------------------------|
Inspect every planned trajectory to verify that it is collision free before executing. By default there are **no obstacles** registered in the motion planning scene. Therefore, trajectories may be planned that intersect the table or other obstacles.

## Velocity Control with Game Controller
You can control the arm using a game controller to direct the linear and angular velocity of the end effector. If you wish to do so, plug in an Xbox controller and ensure that the `use_controller` argument to `bravo_driver.launch` (enabled by default) is `true`:
```
roslaunch raven_manip_sw bravo_driver.launch use_controller:=true
```

The controls are given in `bravo_ros/bravo_moveit_config/config/xbox_controller.yaml`. If you have a different type controller, you should create a different configuration.


## Calibration

To run calibrate the position of the camera relative to the `bravo_base_link`, there are two options - MoveIt HandEye Calibration and a manual tuning method.

Details on running the calibration routine (including parts, installation, and control) are listed in the [wiki](https://gitlab.com/groups/apl-ocean-engineering/raven/manipulation/-/wikis/Bravo_Arm/Calibration).

High-Level:

Run: `roslaunch raven_manip_sw calibration.launch manual_calibration:=true/false` to launch the calibration routine.

### Manual Calibration
Running the manual routine will allow the user to control the 6DOF pose of the trisect using a slider GUI. Once you're happy with the alignment, in a separate terminal run:

```bash
$ rosrun tf2_tools echo.py -l 1 world left

At time 1689788723.9966576, (current time 1689788724.044241)
- Translation: [-0.020, -0.295, 0.462]
- Rotation: in Quaternion [0.006, 0.277, -0.001, 0.961]
            in RPY (radian) [0.014, 0.561, 0.001]
            in RPY (degree) [0.784, 32.156, 0.080]
```

Now update the parameters in the `bravo_description/launch/pub_camera_pose.launch` file:
```
<launch>
    <node name="zed_pose_publisher" pkg="bravo_description" type="pub_cam_position" output="screen" required="true">
        <!-- Camera pose -->
        <param name="cam_x"                 value="-0.020" />
        <param name="cam_y"                 value="-0.295" />
        <param name="cam_z"                 value="0.462" />
        <param name="cam_roll"              value="0.014" />
        <param name="cam_pitch"              value="0.561" />
        <param name="cam_yaw"               value="0.001" />

        <!-- World frame -->
        <param name="world_frame"           value="robot_base_link"/>
        <param name="parent_frame"          value="robot_base_link" />
        <param name="child_frame"           value="left" />
        <param name="camera_base_frame"     value="trisect_origin" />
    </node>
</launch>
```

## Automatic Calibration

Running automatic calibration is more in-depth and covered step-by-step in the [wiki](https://gitlab.com/groups/apl-ocean-engineering/raven/manipulation/-/wikis/Bravo_Arm/Calibration).

The tool does some weird things, including breaking the /tf tree being published by the URDF state publisher and by the `zed_camera_pose` node. That's the reason for the `_dummy` link in the URDF, otherwise the actual URDF is broken when the transform is being solved, resulting in wonky results.

| :exclamation: Warning!|
|-----------------------------------------|
I have yet to get good results using the HandEye Calibration Tool! So kudos if you can get good stuff out. I have been manually calibrating with much better luck.
