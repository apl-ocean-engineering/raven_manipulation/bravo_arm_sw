[[_TOC_]]

# Quickstart
## Running Simulations Remotely
In order to facilitate development on less powerful hardware, you can run ROS across two machines and use a remote, headless server to run the more intense operations.

Prerequisties are:
* Running ROS across multiple machines (intructions [here](https://wiki.ros.org/ROS/Tutorials/MultipleMachines))
* Manipulation stack is setup on both machines

On the server:
```bash
# We need to fake a screen to Gazebo can start
# the camera plugins headless
sudo apt install xvfb # (this is included by default in the docker build)
Xvfb :1 -screen 0 1600x1200x16
export DISPLAY=:1.0

# This will depend on your network setup. For APL zerotier:
echo "10.147.18.74   vespine" >> /etc/hosts
export ROS_MASTER_URI=http://vespine:11311; export ROS_IP=10.147.18.74

# Start the simulation stack sans viz from the src folder
cd raven_manip_sw/launch/launch_scripts

# In tmux, locally
./start_sim_server.sh

# Or in tmux, docker:
./start_sim_server_docker.sh
```

On your local machine:
```bash
# This will depend on your network setup. For APL zerotier:
export ROS_MASTER_URI=http://vespine:11311
# Start the simulation stack, minus visualizations:
# each in separate terminals:
roslaunch raven_manip_sw visualizations.launch simulated:=true start_rqt:=falses
```

### If the Client is a Mac:
The same server instructions apply. However, we need to run ROS Noetic inside a docker and export some different environmental variables.

 - Install xquartz: `brew install --cask xquartz`
   - Allow network connections: ![macos](../figs/xquartz.png)
- Run `export DISPLAY=$(ifconfig en0 | awk '/inet /{print $2 ":0"}')` to set the display properly
- In `docker/docker-compose/noetic` run `docker compose -f docker-compose-macos.yaml up` to start a Noetic image locally.
   - Connect to the image via `docker exec -it noetic-noetic-1 /bin/bash`

### xvfb Errors
Running the Xvfb command can return errors like these which indicates the server is already running:

```bash
_XSERVTransSocketUNIXCreateListener: ...SocketCreateListener() failed
_XSERVTransMakeAllCOTSServerListeners: server already running
(EE)
Fatal server error:
(EE) Cannot establish any listening sockets - Make sure an X server isn't already running(EE)
```
If it is running, you can safely ignore this.

If you see an error like this:

![manip_ui](../figs/gazebo_error.png)

It indicates that you need to re-run the command (or re-export the DISPLAY variable).

## Start everthing for the simulated arm

### Using tmux:
```bash
cd raven_manip_sw/launch/launch_scripts

# In one terminal
./start_manip_sim.sh
```

### Not Using tmux:
```bash
# Each in their own terminal:
roslaunch raven_manip_sw bravo_arm.launch simulated:=true

roslaunch raven_manip_sw perception.launch simulated:=true start_filter:=true

roslaunch raven_manip_sw grasping.launch

roslaunch raven_manip_sw motion_control.launch

roslaunch manip_executive manip_executive.launch

rosrun manip_executive manip_ui.py
```

### Start everything in the tank:
```bash
# Each in their own terminal:
roscore

roscd libbpl_protocol
./bpl_watchdogd/build/bpl_watchdogd

# ssh into trisect, start camera
ssh trisect@trisect1
./start_trisect.sh # still in trisect

# back on main computer
export PYTHONPATH=${PYTHONPATH}:~/py_trees
roslaunch raven_manip_sw visualizations.launch

roslaunch raven_manip_sw bravo_arm.launch

roslaunch raven_manip_sw perception.launch start_lights:=true start_filter:=true

roslaunch raven_manip_sw grasping.launch

roslaunch motion_executive motion_server.launch

roslaunch manip_executive manip_executive.launch

rosrun manip_executive manip_ui.py
```

# Running a simulation

After starting all of the launch files, the grasping pipeline waits for human input to step through the stages.

Use the `manip_ui.py` pane to provide that input, while looking at the `manip_executive.launch` pane to see the current state of the behavior tree. Errors are not currently surfaced, and will result in the tree resetting to its initial state; in that case, look at the individual launchfile panes to find error messages.

![manip_ui](../figs/manip_ui.png)
* Right now, we only support "Task 1 (grasp)" for the "kettlebell" object, so select that
* Click "save pointcloud" when happy with the test setup
* Select Grasp; use 0 to go with whichever the current selection algorithm prioritized
* Look in rviz to confirm that the plan is safe. This is particularly important when using the physical arm!

## Gazebo interactions

There are several ways to modify/reset the gazebo simulation environment.

1. To reset the simulation, `rosservice call /gazebo/reset_world`. This will put the kettlebell back where it started. Do NOT call `/gazebo/reset_simulation`, since this will also reset the controllers and cause the control chain to freak out.
2. To move the kettlebell around, you can use the gazebo GUI. Run the launch file with start_gazebo:=true (it defaults to false, which runs Gazebo in the background which is much more efficient.) Within the GUI, pause time so gravity isn't acting on the object, then use the interactive marker handles to drag it where you want it.


## Relevant Parameters

### Grasp Selection

Grasp selection is an active area of research for the group, and we have a number of different approaches for it.
This can most easily be configured using *dynamic reconfigure* in *rqt*.
(There are also parameters in `grasp_selection.launch`, but they're not exposed in the top-level `grasping.launch`.)

There are 4 choices for filter_method:
1. **max_conf**: simply filters on confidences provided by tsgrasp. Will often result in nearly identical grasp choices being presented.
2. **mcd**: deprecated. Early attempt at enforcing spatial diversity between grasp options.
3. **cost_function**: applies a cost function with three components, where the individual weights are also parameters:
  * angle_weight: this is the somewhat hacky way that we specify which quadrant to grab the object in in order to enforce visibility.
  * conf_weight: directly from tsgrasp
  * manip_weight: manipulability of pose at grasp; often mimics angle_weight
4. **clusters**: active research; slower than cost_function

# Launch File Organization
To help organize the various meta-repos/launch files, see the list below for an explanation of what to run.

A normal launch (to run a task, for instance) would consist of launching
* `bravo_arm.launch`
* `perception.launch`
* `grasp_synthesis.launch`
* `motion_controller.launch`
This would be sufficient to execute a grasp.

Sort of ordered in terms of importance:

### `bravo_arm.launch`
**Required Repos**: [libbpl_protocol](https://gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis), [bravo_ros](https://gitlab.com/apl-ocean-engineering/raven/manipulation/bravo_ros), [rsa_bravo_driver](https://gitlab.com/rsa-manipulation-testbed/rsa_bravo_driver)

This launch file runs the arm (same as `arm_only.launch`). Also includes the camera nodes, the MoveIt planning and control nodes (including MoveIt Servo), the node to add a game controller, and visualization (RViz).

**This node is the important node to run for arm control.**

### `arm_only.launch`
**Required Repos**: [libbpl_protocol](https://gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis), [bravo_ros](https://gitlab.com/apl-ocean-engineering/raven/manipulation/bravo_ros), [rsa_bravo_driver](https://gitlab.com/rsa-manipulation-testbed/rsa_bravo_driver)

This launch file is for testing the arm communication (does NOT run the MoveIt planning/control nodes)


### `grasping.launch`
**Required Repos**: [grasp_synthesis](https://gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis), [raven_manip_msgs](https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_msgs)

This launch file runs the TSGrasp network (inside a docker container). Also runs the `grasp_filter_node` and visualizations.

### `motion_control.launch`
**Required Repos**: [motion_executive](https://gitlab.com/apl-ocean-engineering/raven/manipulation/motion_executive), [raven_manip_msgs](https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_msgs)

This launch file launches the state machine GUI and controls the MoveIt pipeline. Useful for actually running the demos to link the various pipelines together.

### `perception.launch`
**Required Repos**: [object_detection](https://gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection), [raven_manip_msgs](https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_msgs)

This launch file runs the object detector (inside a docker container). Also runs a `crop_pointcloud.py` node that publishes the 3D detection bound box.

### `visualiations.launch`

Runs the rqt and rviz nodes.

### `zed_only.launch`
**Required Repos**: [bravo_ros](https://gitlab.com/apl-ocean-engineering/raven/manipulation/bravo_ros), [zed_wrapper](https://github.com/stereolabs/zed-ros-wrapper)

Runs the zed camera node. Also runs a camera pose publishing node that publishes a /tf giving the relative transform between the robot and the camera. Not often run by itself (although if you're debugging object detection, this can be a lightweight option instead of running the arm-related nodes).
