[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
# raven_manip_sw

Launch files for running the Blueprint Labs Bravo in simulation or in the tank. This README serves as the starting point for much of OPAL's manipulation work.

* [installation instructions](docs/installation.md)
* [getting started](docs/getting_started.md) -- this includes notes about software organization and running the code
* Info about working with the [arm hardware](docs/arm_hardware.md)
