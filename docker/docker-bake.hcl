# docker buildx bake STAGE --load
# docker buildx bake core --set core.platform=linux/amd64 --load
group "default" {
  targets = ["core", "base"]
}

target "core" {
  target = "core"
  context    = "."
  dockerfile = "Dockerfile"
  platforms  = ["linux/amd64", "linux/arm64"]
  tags       = [
    "registry.gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw/noetic:amd64",
    "registry.gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw/noetic:arm64"
  ]
}

target "base" {
  target = "base"
  context    = "."
  dockerfile = "Dockerfile"
  platforms  = ["linux/amd64"]
  tags       = [
    "registry.gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw/raven_manip_sw:amd64",
  ]
  output = ["type=docker"]
}
