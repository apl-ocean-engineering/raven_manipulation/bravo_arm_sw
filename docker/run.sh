#!/bin/bash

# Environment variables
DISPLAY_ENV=${DISPLAY}
XSOCK=/tmp/.X11-unix
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

xhost +local:docker
docker run -itd \
    --privileged --network host -e DISPLAY=${DISPLAY_ENV} \
    -v ${XSOCK}:${XSOCK}:rw \
    -v $SCRIPT_DIR/../../../src:/raven_manip_ws/src \
    --name manipulation_docker \
    raven_manip_sw:latest /bin/bash
