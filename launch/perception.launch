<?xml version="1.0"?>
<!-- A top-level launch file to start the perception services
    * Detector Node
    * Point Cropping Node
    * Light control (in HH101)
    * Aruco detection
    * Octomap for collision
-->
<launch>
    <arg name="simulated"
        default="false" />

    <arg name="start_stereo"
        default="$(eval not simulated)"
        doc="Whether to start a stereo pipeline." />

    <arg name="stereo_method"
        default="raft"
        doc="Stereo method - options are [raft, ros, vpi]." />

    <arg name="start_detector"
        default="true"
        doc="Whether to start an object detection pipeline." />

    <arg name="detector"
        default="detectron2"
        doc="Which detector to use (yolov8, detectron2)" />

    <arg name="start_aruco"
        default="false"
        doc="Whether to start an aruco detector." />

    <arg name="start_cropper"
        default="true"
        doc="Whether to start a pc cropping node." />

    <arg name="start_lights"
        default="false"
        doc="Whether to start the hh101 light driver." />

    <arg name="start_filter"
        default="true"
        doc="Whether to start self-filtering pointcloud." />

    <arg name="start_octomap"
        default="false"
        doc="Whether to start an octomap node for collision checking." />

    <arg name="test_setup"
        default="hh101"
        doc="[hh101, hh101_testbed, hh104]" />

    <!-- Start stereo node (useful if running with raven recorded data) -->
    <arg name="run_docker" default="true"
            doc="If true, the GPU-accelerate VPI matching is run in a Docker image.
            If false, a local (non-Docker) version of ROS stereo_image_proc is run instead." />

    <group if="$(arg start_stereo)">
        <group unless="$(arg simulated)">
            <!-- Valid values are:
                    "raft"  Uses RAFT Stereo
                    "ros"   Uses ROS stereo_image_proc
                    "vpi"   Uses VPI (requires Nvidia libraries) -->

            <group if="$(eval stereo_method == 'raft')">
                <include file="$(find raft_stereo)/launch/raft_stereo.launch">
                </include>
            </group>

            <group unless="$(eval stereo_method == 'raft')">
                <arg name="stereo_matcher" default="vpi" doc="`ros` uses ROS stereo_image_proc. `vpi` Uses VPI (requires Nvidia libraries)"/>
                <!-- "downsample" is given as a power of 2 (e.g. downsample = 2 means scale by 4)-->
                <arg name="downsample" default="2" doc="given as a power of 2 (e.g. downsample = 2 means scale by 4)"/>

                <!-- If false, doesn't publish a pc2. Probably not desired behavior -->
                <arg name="run_point_cloud" default="true" doc="publish a pointcloud2"/>

                <include  file="$(find opal_stereo_proc)/launch/opal_stereo_proc.launch">
                    <arg name="run_docker" value="$(arg run_docker)"/>
                    <!-- opal_stereo_proc defaults to ROS, use vpi if running in docker -->
                    <arg name="stereo_matcher" value="$(arg stereo_matcher)"/>
                    <arg name="downsample" value="$(arg downsample)"/>
                    <arg name="run_point_cloud"  value="$(arg run_point_cloud)"/>
                </include>
            </group>
        </group>
    </group>

    <!-- Same for Both -->
    <arg name="filtered_depth_image" default="/realtime_urdf_filter/depth"/>
    <arg name="filtered_pointcloud" default="/filtered_depth/points"/>

    <!-- Simulated -->
    <arg name="use_disparity" default="false" if="$(arg simulated)"/>
    <arg name="image" default="/trisect/left/image_rect" if="$(arg simulated)"/>
    <arg name="camera_info" default="/trisect/left/camera_info" if="$(arg simulated)"/>
    <arg name="depth_image" default="/trisect/depth/depth_registered" if="$(arg simulated)"/>
    <arg name="pointcloud" default="/trisect/point_cloud/cloud_registered" if="$(arg simulated)"/>

    <!-- real -->
    <arg name="use_disparity" default="false" unless="$(arg simulated)"/> <!-- Trisect publishes disparity, raft publishes both -->
    <arg name="image" default="/trisect/downsampled/left/image_rect" unless="$(arg simulated)"/>
    <arg name="camera_info" default="/trisect/downsampled/left/camera_info" unless="$(arg simulated)"/>
    <!-- <arg name="depth_image" default="/trisect/downsampled/disparity" unless="$(arg simulated)"/>
    <arg name="pointcloud" default="/trisect/downsampled/points2" unless="$(arg simulated)"/> -->
    <arg name="depth_image" default="/raft_stereo/depth_image" unless="$(arg simulated)"/>
    <arg name="pointcloud" default="/raft_stereo/pointcloud" unless="$(arg simulated)"/>

    <!-- avoids remapping inside a docker which has been irksome   -->
    <node name="republish_detection" type="republish" pkg="image_transport" args="raw in:=$(arg image) out:=/detection_image" />

    <!-- Using Object Detector -->
    <group if="$(arg start_detector)">
        <!-- Start Object Detector -->
        <!-- Get detector metadata path -->
        <arg name="metadata_path" if="$(eval detector == 'detectron2')"  value="models/detectron2/metadata.yaml" />
        <arg name="metadata_path" if="$(eval detector == 'yolov8')" value="models/yolov8/metadata.yaml" />

        <!-- This uses a trained detection model -->
        <include file="$(find object_detection)/launch/detector_docker.launch">
            <arg name="detector" value="$(arg detector)"/>
            <arg name="model_metadata" value="$(arg metadata_path)"/>
            <arg name="confidence_threshold" value="0.75"/>
            <arg name="verbose" value="false"/>
        </include>
    </group>

    <!-- Start PC Cropper -->
    <!-- Pointcloud Cropper Args -->
    <!-- CLASS IDs:
        0: Kettlebell
        1: Fishtail
    -->
    <arg name="class_id" default="0"/>
    <arg name="verbose" default="false"/>
    <arg name="depth_multiplier" default="1.0"/>
    <arg name="max_depth" default="2"/>
    <remap from="/camera_info" to="$(arg camera_info)" />
    <remap from="/depth_image" to="$(arg depth_image)" />
    <remap from="/pointcloud" to="$(arg pointcloud)" />
    <node pkg="nodelet" type="nodelet" args="manager" name="standalone_nodelet" output="screen"/>

    <!-- Launch the point_cloud_cropper node -->
    <node if="$(arg start_cropper)" name="bbox_detector" pkg="object_detection" type="bbox_detection_node.py" output="screen" respawn="false">
        <param name="max_depth" value="$(arg max_depth)"/>
        <param name="class_id" value="$(arg class_id)"/>
        <param name="use_disparity" value="$(arg use_disparity)"/>
        <param name="verbose" value="$(arg verbose)"/>
        <param name="depth_multiplier" value="$(arg depth_multiplier)"/>
        <remap from="/detections" to="/object_detector/detections" />
    </node>

    <!-- Using Realtime URDF Filter -->
    <group if="$(arg start_filter)">
        <arg name="config_file" default="$(find raven_manip_sw)/params/self_filter_params_$(arg test_setup).yaml" if="$(arg simulated)" />
        <arg name="config_file" default="$(find raven_manip_sw)/params/self_filter_params_real.yaml" unless="$(arg simulated)" />

        <!-- Launch nodelet -->
        <node pkg="nodelet" type="nodelet" args="manager" name="nodelet_manager" output="screen"/>
        <node pkg="nodelet" type="nodelet" name="realtime_urdf_filter"
                args="load realtime_urdf_filter/RealtimeURDFFilterNodelet nodelet_manager"
                output="screen">
            <remap from="~input_depth" to="$(arg depth_image)" />
            <remap from="~input_points" to="$(arg pointcloud)" />
            <remap from="~output_depth" to="~filtered_depth/depth" />
            <remap from="~output_mask" to="~filtered_depth/mask" />
            <rosparam command="load" file="$(arg config_file)"/>
        </node>

        <node name="filter_operations_node" pkg="object_detection" type="filter_operations_node.py" output="screen" respawn="false" launch-prefix="/usr/bin/python3">
            <param name="dilation_kernel_size" value="5"/>
            <param name="dilation_iters" value="2"/>
            <param name="erosion_kernel_size" value="5"/>
            <param name="erosion_iters" value="1"/>

            <remap from="input_depth" to="$(arg depth_image)" />
            <remap from="object_mask" to="/bbox_detector/object_mask_gradient" />
            <remap from="self_mask" to="/realtime_urdf_filter/filtered_depth/mask" />
            <remap from="camera_info" to="$(arg camera_info)" />
        </node>

    </group>

    <group if="$(arg start_octomap)">
        <node name="background_octomap" pkg="octomap_server" type="octomap_server_node" output="screen" respawn="false">
            <param name="resolution" value="0.05" />
            <param name="frame_id" type="string" value="map" />
            <param name="latch"  value="false" />
            <param name="sensor_model/max_range" value="1.5"/>
            <param name="sensor_model/min" value="0.12"/>
            <param name="sensor_model/max" value="0.97"/>
            <param name="sensor_model/hit" value="0.8"/>
            <param name="sensor_model/miss" value="0.1"/>
            <remap from="cloud_in" to="/filtered_background/points" if="$(arg start_filter)" />
            <remap from="cloud_in" to="$(arg pointcloud)"  unless="$(arg start_filter)" />
            <remap from="octomap_binary" to="/filtered_background/octomap_binary" />
            <remap from="octomap_full" to="/filtered_background/octomap_full" />

        </node>

        <node if="$(arg start_filter)" name="object_octomap" pkg="octomap_server" type="octomap_server_node" output="screen" respawn="false">
            <param name="resolution" value="0.005" />
            <param name="frame_id" type="string" value="map" />
            <param name="latch"  value="false" />
            <param name="sensor_model/max_range" value="1.5"/>
            <param name="sensor_model/min" value="0.12"/>
            <param name="sensor_model/max" value="0.97"/>
            <param name="sensor_model/hit" value="0.7"/>
            <param name="sensor_model/miss" value="0.2"/>
            <remap from="cloud_in" to="/filtered_object/points" />
            <remap from="octomap_binary" to="/filtered_object/octomap_binary" />
            <remap from="octomap_full" to="/filtered_object/octomap_full" />
        </node>
    </group>

  </launch>
