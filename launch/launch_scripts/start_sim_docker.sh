#! /usr/bin/env bash

# Script that starts a tmux session with tabs pre-populated to run
# the grasping pipeline in simulation in docker
#
# Must be run from the launch/launch_scripts directory since it relies on relative paths to find
# scripts.

# Start the raven_manip_sw docker container in the background
xhost +local:docker # Allow GUI access

containers=("manipulation" "detection" "grasp_synthesis" "mapping")
warning_needed=false

# Loop through each container and check if it's running
for container in "${containers[@]}"; do
    container_status=$(docker ps --filter "name=$container" --format "{{.Status}}")

    if [[ -n "$container_status" ]]; then
        echo "The container '$container' is running."
    else
        echo "The container '$container' is not running."
        warning_needed=true
    fi
done

# If any container is not running, print a warning
if $warning_needed; then
    echo "ERROR: One or more containers are not running."
    echo "Run: docker compose up -d from inside the docker-compose/dev folder!"
    exit 1
fi

# Create new tmux session
# -2 => enable 256 color support, useful for `mon launch` and `manip_ui.py`
# -s manip_docker => names the session "manip_docker"
# -d => prevents script from immediately joining session
tmux -2 new-session -s manip_docker -d

# For each window this script creates, we want to source the manipulation
# workspace and pre-populate the terminal with the command to be executed.
# We don't automatically execute the commands (except roscore)
tmux new-window -t manip_docker:1 -n roscore
tmux set-option -t manip_docker:1 remain-on-exit on
tmux send-keys -t manip_docker:1 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:1 'export ROS_MASTER_URI=http://vespine:11311; export ROS_IP=10.147.18.74; export DISPLAY=:1' Enter
tmux send-keys -t manip_docker:1 'roscore' Enter

# `mon launch` is syntactic sugar for `rosrun rosmon_core rosmon`
# For some reason, it only works if we source setup.bash *after* starting /bin/bash
# I haven't figured out why, but using the long version or using send-keys both work
tmux new-window -t manip_docker:2 -n bravo_arm
tmux set-option -t manip_docker:2 remain-on-exit on
tmux send-keys -t manip_docker:2 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:2 'export TERM=xterm-256color; export DISPLAY=:1' Enter
tmux send-keys -t manip_docker:2 "rosrun rosmon_core rosmon raven_manip_sw bravo_arm.launch simulated:=true start_rviz:=false test_setup:=hh104"

# Create rviz pane
tmux new-window -t manip_docker:3 -n rviz
tmux set-option -t manip_docker:3 remain-on-exit on
tmux send-keys -t manip_docker:3 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:3 'export TERM=xterm-256color; export DISPLAY=:1' Enter
tmux send-keys -t manip_docker:3 "rviz -d src/bravo_ros/bravo_gazebo/rviz/bravo_sim.rviz"

# Create Perception Tab
tmux new-window -t manip_docker:4 -n perception
tmux set-option -t manip_docker:4 remain-on-exit on
tmux split-window -h -t manip_docker:4

# Perception Left Pane (Run detection docker)
tmux send-keys -t manip_docker:4.0 'docker exec -it detection /bin/bash' Enter
tmux send-keys -t manip_docker:4.0 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:4.0 'source /object_detection/object_detection_ws/devel/setup.bash && rosrun --prefix $NN_CONDA_PATH object_detection detector_node.py' Enter

# Perception Right Pane
tmux send-keys -t manip_docker:4.1 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:4.1 'export TERM=xterm-256color; export DISPLAY=:1' Enter
tmux send-keys -t manip_docker:4.1 'rosrun rosmon_core rosmon raven_manip_sw perception.launch simulated:=true start_filter:=true start_detector:=false'

# Create Mapping Tab
tmux new-window -t manip_docker:5 -n mapping
tmux set-option -t manip_docker:5 remain-on-exit on
tmux split-window -h -t manip_docker:5

# Mapping Left Pane (Run mapping docker)
tmux send-keys -t manip_docker:5.0 'docker exec -it mapping /bin/bash' Enter
tmux send-keys -t manip_docker:5.0 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:5.0 'source /mapping_ws/devel/setup.bash' Enter

# Mapping Right Pane
tmux send-keys -t manip_docker:5.1 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:5.1 'export TERM=xterm-256color; export DISPLAY=:1' Enter

# Create Grasping Tab
tmux new-window -t manip_docker:6 -n grasping
tmux set-option -t manip_docker:6 remain-on-exit on
tmux split-window -h -t manip_docker:6

# Grasping Left Pane (Run grasping docker)
tmux send-keys -t manip_docker:6.0 'docker exec -it grasp_synthesis /bin/bash' Enter
tmux send-keys -t manip_docker:6.0 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:6.0 'source /grasp_synth/grasp_synth_ws/devel/setup.bash && rosrun --prefix $NN_CONDA_PATH grasp_synthesis grasp_synthesis_node.py' Enter

# Grasping Right Pane
tmux send-keys -t manip_docker:6.1 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:6.1 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:6.1 "rosrun rosmon_core rosmon raven_manip_sw grasping.launch start_synthesis:=false"

# Create Motion Control Pane
tmux new-window -t manip_docker:7 -n motion
tmux set-option -t manip_docker:7 remain-on-exit on
tmux send-keys -t manip_docker:7 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:7 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:7 "rosrun rosmon_core rosmon raven_manip_sw motion_control.launch"

# Intentionally leave this using roslaunch; the ASCII drawing of the tree
# doesn't benefit from being started in `mon launch`
tmux new-window -t manip_docker:8 -n manip
tmux set-option -t manip_docker:8 remain-on-exit on
tmux split-window -h -t manip_docker:8

# Manip Left Pane (Manip Executive)
tmux send-keys -t manip_docker:8.0 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:8.0 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:8.0 "roslaunch manip_executive manip_executive.launch"

# Manip Right Pane (Manip UI)
tmux send-keys -t manip_docker:8.1 'docker exec -it manipulation /bin/bash' Enter
tmux send-keys -t manip_docker:8.1 'export TERM=xterm-256color' Enter
tmux send-keys -t manip_docker:8.1 "rosrun manip_executive manip_ui.py"

tmux select-window -t manip_docker:1
tmux attach-session -t manip_docker
