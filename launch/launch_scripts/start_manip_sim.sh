#! /usr/bin/env bash

# Script that starts a tmux session with tabs pre-populated to run
# the grasping pipeline in simulation
#
# Must be run from the launch/launch_scripts directory since it relies on relative paths to find
# scripts.

# Create new tmux session
# -2 => enable 256 color support, useful for `mon launch` and `manip_ui.py`
# -s manip => names the session "manip"
# -d => prevents script from immediately joining session
tmux -2 new-session -s manip -d

# For each window this script creates, we want to source the manipulation
# workspace and pre-populate the terminal with the command to be executed.
# We don't automatically execute the commands
tmux new-window -t manip:1 -n roscore -c '../../../..' 'source devel/setup.bash; /bin/bash'
tmux set-option -t manip:1 remain-on-exit on
tmux send-keys -t manip:1 "roscore"

# `mon launch` is syntactic sugar for `rosrun rosmon_core rosmon`
# For some reason, it only works if we source setup.bash *after* starting /bin/bash
# I haven't figured out why, but using the long version or using send-keys both work
tmux new-window -t manip:2 -n bravo_arm -c '../../../..' '/bin/bash'
tmux set-option -t manip:2 remain-on-exit on
tmux send-keys -t manip:2 'source devel/setup.bash' Enter
tmux send-keys -t manip:2 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:2 "rosrun rosmon_core rosmon raven_manip_sw bravo_arm.launch simulated:=true start_rviz:=false test_setup:=hh104"

# Create rviz pane
tmux new-window -t manip:3 -n rviz -c '../../../..' '/bin/bash'
tmux set-option -t manip:3 remain-on-exit on
tmux send-keys -t manip:3 'source devel/setup.bash' Enter
tmux send-keys -t manip:3 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:3 "rviz -d src/bravo_ros/bravo_gazebo/rviz/bravo_sim.rviz"

# Create Perception Tab
tmux new-window -t manip:4 -n perception -c '../../../..' '/bin/bash'
tmux set-option -t manip:4 remain-on-exit on
tmux send-keys -t manip:4 'source devel/setup.bash' Enter
tmux send-keys -t manip:4 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:4 "rosrun rosmon_core rosmon raven_manip_sw perception.launch simulated:=true start_filter:=true test_setup:=hh104"

# Create Mapping Tab
tmux new-window -t manip:5 -n mapping -c '../../../../src/mapping' '/bin/bash'
tmux set-option -t manip:5 remain-on-exit on
tmux send-keys -t manip:5 'docker exec -it mapping /bin/bash' Enter
tmux send-keys -t manip:5 'source /mapping_ws/devel/setup.bash' Enter
tmux send-keys -t manip:5 "mon launch mapping nvblox_ros.launch"

# Create Grasping Tab
tmux new-window -t manip:6 -n grasping -c '../../../..' '/bin/bash'
tmux set-option -t manip:6 remain-on-exit on
tmux send-keys -t manip:6 'source devel/setup.bash' Enter
tmux send-keys -t manip:6 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:6 "rosrun rosmon_core rosmon raven_manip_sw grasping.launch"

# Create Motion Control Pane
tmux new-window -t manip:7 -n motion -c '../../../..' '/bin/bash'
tmux set-option -t manip:7 remain-on-exit on
tmux send-keys -t manip:7 'source devel/setup.bash' Enter
tmux send-keys -t manip:7 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:7 "rosrun rosmon_core rosmon raven_manip_sw motion_control.launch"

# Intentionally leave this using roslaunch; the ASCII drawing of the tree
# doesn't benefit from being started in `mon launch`
tmux new-window -t manip:8 -n manip -c '../../../..' '/bin/bash'
tmux set-option -t manip:8 remain-on-exit on
tmux split-window -h -t manip:8 -c '../../../../' '/bin/bash'

# Manip Left Pane (Manip Executive)
tmux send-keys -t manip:8.0 'source devel/setup.bash' Enter
tmux send-keys -t manip:8.0 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:8.0 "roslaunch manip_executive manip_executive.launch"

# Manip Right Pane (Manip UI)
tmux send-keys -t manip:8.1 'source devel/setup.bash' Enter
tmux send-keys -t manip:8.1 'export TERM=xterm-256color' Enter
tmux send-keys -t manip:8.1 "rosrun manip_executive manip_ui.py"

tmux select-window -t manip:1
tmux attach-session -t manip
