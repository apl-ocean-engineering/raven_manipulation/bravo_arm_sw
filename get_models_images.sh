# This script is expected to be run from within the raven_manip_sw folder
# Pull manipulation image
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw/raven_manip_sw:latest

# Pull Object Detection
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection/detection:base
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection/detection:node

# Pull Grasp Synthesis Images
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis/grasp_synthesis:base
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis/grasp_synthesis:node

# Pull Raft Stereo (Not Required for Simulation!)
docker pull registry.gitlab.com/apl-ocean-engineering/raft_stereo/raft_stereo:latest

# Pull nvblox Image:
docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/mapping/nvblox:base

# Get Detectron2 Model
wget -O ../object_detection/models/detectron2/model.pth https://gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection/-/package_files/159989830/download

# Get tsgrasp model
wget -O ../grasp_synthesis/src/grasp_synthesis/tsgrasp/models/tsgrasp_scene_1_frame/model.ckpt https://gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis/-/package_files/160055853/download
